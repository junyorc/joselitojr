
package pagina;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;






public class CampoDeTreinamento {

	@Test
	public void CadastroSimples() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\aluno\\Downloads\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.manage().window() .maximize();

		driver.get("file:///C:/Users/aluno/eclipse-workspace/Teste/componentes.html");

		WebElement cadastrar = driver.findElement(By.id("elementosForm:cadastrar"));
		cadastrar.click();
		Alert alertCadastrar = (Alert) driver.switchTo() .alert();
		String texto = alertCadastrar.getText();
		Assert.assertEquals("Nome eh obrigatorio",texto);
		alertCadastrar.accept();
		driver.switchTo().window("");

		WebElement nome = driver.findElement(By.id("elementosForm:nome"));
		nome.clear();
		nome.sendKeys("Joselito");

		WebElement sobrenome = driver.findElement(By.id("elementosForm:sobrenome"));
		sobrenome.clear();
		sobrenome.sendKeys("Junior");

		WebElement sexo = driver.findElement(By.id("elementosForm:sexo:0"));
		sexo.click();

		WebElement comida = driver.findElement(By.id("elementosForm:comidaFavorita:0"));
		comida.click();

		WebElement comida2 = driver.findElement(By.id("elementosForm:comidaFavorita:1"));
		comida2.click();

		WebElement comida3 = driver.findElement(By.id("elementosForm:comidaFavorita:2"));
		comida3.click();

		WebElement escolaridade = driver.findElement(By.id("elementosForm:escolaridade"));
		Select combo = new Select(escolaridade);
		combo.selectByValue("doutorado");
		Assert.assertEquals("Doutorado", combo.getFirstSelectedOption().getText());

		WebElement esporte = driver.findElement(By.id("elementosForm:esportes"));
		Select combo2 = new Select(esporte);
		combo2.selectByValue("Karate");
		Assert.assertEquals("Karate", combo2.getFirstSelectedOption().getText());

		WebElement sugestao = driver.findElement(By.id("elementosForm:sugestoes"));
		sugestao.clear();
		sugestao.sendKeys("Nada a Declarar");

		WebElement cadastrar2 = driver.findElement(By.id("elementosForm:cadastrar"));
		cadastrar2.click();






	}



}










